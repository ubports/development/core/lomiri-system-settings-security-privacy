# Security and privacy settings plugin for Lomiri system settings

lomiri-system-settings-security-privacy is now merged back to the main
lomiri-system-settings repository in [lomiri-system-settings!441]. Please see
[here] for current code.

Ubuntu Touch based on Ubuntu 20.04 still uses code in this repository in the
`ubports/focal` branch.

[lomiri-system-settings!441]: https://gitlab.com/ubports/development/core/lomiri-system-settings/-/merge_requests/441
[here]: https://gitlab.com/ubports/development/core/lomiri-system-settings/-/tree/main/plugins/security-privacy?ref_type=heads
